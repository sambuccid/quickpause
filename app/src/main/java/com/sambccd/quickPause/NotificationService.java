package com.sambccd.quickPause;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.hardware.display.DisplayManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.provider.Settings;
import android.view.Display;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.sambccd.quickPause.attempts.AttemptRecords;
import com.sambccd.quickPause.util.Log;
import com.sambccd.quickPause.util.NotificationIdGenerator;
import com.sambccd.quickPause.util.SettingUtils;
import com.sambccd.quickPause.util.TimeScheduleUtil;
import com.sambccd.quickPause.util.TimeUtil;

import java.util.Calendar;
import java.util.concurrent.atomic.AtomicBoolean;

public class NotificationService extends Service {
    public static final int secsAvailableToSwitchOff =7;
    public static final String notificationChannelId = "alertNotificationChannel";
    public static final long millisToPassForCheckScreenKeepingOff=500;
    public static final long millisToPassForCheckScreenGoneOff=2000;
    public NotificationService() {
    }
    @Override
    public IBinder onBind(Intent intent) {return null;/*not used as bind service, but as start one*/}

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.log("notificationService","onStartCommand");
        manageNotification(startId);
        Log.log("notificationService","finish onStartCommand");
        return Service.START_NOT_STICKY;
    }
    private void manageNotification(int startId){
        NotificationCompat.Builder notificationBuilder = createNotificationBuilder();
        int notificationId= NotificationIdGenerator.generateId();
        createNotificationChannel();
        startForeground(notificationId, notificationBuilder.build());
        if(SettingUtils.vibrationAtStart(getApplicationContext())){
            notifyStartWithVibration();
        }
        Log.log("notificationService","sned notification with foreground");
        createCallbackForNotificationUpdate(notificationId,notificationBuilder,startId);
    }
    private NotificationCompat.Builder createNotificationBuilder() {
        String notificationMessage = getString(R.string.notificationMessage, secsAvailableToSwitchOff);
        return new NotificationCompat.Builder(getApplicationContext(),notificationChannelId)
                .setSmallIcon(R.drawable.notification_icon_4_2)
                .setContentTitle(getString(R.string.notificationTitle))
                .setContentText(notificationMessage)
                .setPriority(NotificationCompat.PRIORITY_HIGH);
    }
    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "Alert notification screen off";
            String description = getString(R.string.notificationChannelDescription);
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel channel = new NotificationChannel(notificationChannelId, name, importance);
            channel.setDescription(description);
            NotificationManager notificationManager = getApplicationContext().getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }
    private AtomicBoolean screenCheckStarted=new AtomicBoolean();
    private AtomicBoolean challengeFinished=new AtomicBoolean(false);
    private void createCallbackForNotificationUpdate(final int notificationId, final NotificationCompat.Builder notificationBuilder, final int startId) {
        Handler handler = new Handler();
        screenCheckStarted.set(false);
        for(int i=1;i<secsAvailableToSwitchOff;i++){
            int secondsLeft = secsAvailableToSwitchOff-i;
            String message=getString(R.string.notificationMessage, secondsLeft);
            NotificationCountdown delayedNotifUpdate=new NotificationCountdown(message,notificationId,startId,notificationBuilder,false);
            handler.postDelayed(delayedNotifUpdate,i*1000);
        }
        String finalMessage = getString(R.string.notificationMessageEnd);
        NotificationCountdown lastDelayedNotifUpdate= new NotificationCountdown(finalMessage,notificationId,startId,notificationBuilder,true);
        handler.postDelayed(lastDelayedNotifUpdate,secsAvailableToSwitchOff*1000);
    }
    private void startCheckIfScreenOffAndCheckNotStarted(final int notificationId,final int startId){
        DisplayManager dm = displayManager();
        if(isScreenOff(dm)){
            boolean prevCheckStartedValue = screenCheckStarted.getAndSet(true);
            if(!prevCheckStartedValue){
                startScreenCheck(notificationId,startId);
            }
        }
    }
    private void startScreenCheck(final int notificationId,final int startId){
        Thread thread = new Thread(new Runnable() {
            @Override public void run() {
                long nowAtStart = Calendar.getInstance().getTimeInMillis();
                long millisToKeepOff = TimeScheduleUtil.secondsToMills(SettingUtils.getSecondsScreenOff(getApplicationContext()));
                long whenTimeWillBePassed=nowAtStart + millisToKeepOff;
                DisplayManager dm = displayManager();
                boolean screenKeptOff=true;
                while(!TimeUtil.momentPassed(whenTimeWillBePassed) && screenKeptOff){
                    if(!isScreenOff(dm)) {
                        screenKeptOff=false;
                    }
                }
                if(screenKeptOff){
                    notifySuccess(notificationId);
                    success();
                }else{
                    notifyNoSuccess(notificationId);
                    noSuccess();
                }
                final Handler mainHandler = new Handler(Looper.getMainLooper());
                mainHandler.post(new Runnable() {
                    @Override public void run() {
                        stop(startId);
                    }
                });
            }
        });
        thread.start();
    }
    private void updateNotification(NotificationCompat.Builder builder, int notifId,String message){
        builder.setContentText(message);
        builder.setOnlyAlertOnce(true);
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(getApplicationContext());
        notificationManager.notify(notifId,builder.build());
    }
    private void notifySuccess(int notifId){
        SettingUtils.WayNotifyEndChallenge wayToNotifyEndChallenge = SettingUtils.getWayNotifyEndChallenge(getApplicationContext());
        if(wayToNotifyEndChallenge.soundEnable) {
            Uri notificationSound = Settings.System.DEFAULT_NOTIFICATION_URI;
            Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notificationSound);
            r.play();
        }
        if(wayToNotifyEndChallenge.vibrationEnable) {
            Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                v.vibrate(VibrationEffect.createOneShot(500, VibrationEffect.DEFAULT_AMPLITUDE));
            } else {
                v.vibrate(500);
            }
        }
        Log.log("notificationService","notify timer finish");
    }
    private void notifyNoSuccess(int notifId){
        long[] pattern={0,100,100,100,100,100};
        Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            v.vibrate(VibrationEffect.createWaveform(pattern, -1));
        } else {
            v.vibrate(pattern,-1);
        }
        Log.log("notificationService","notify timer finish");
    }
    private void notifyStartWithVibration(){
        long[] pattern={0,100,50,100};
        Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            v.vibrate(VibrationEffect.createWaveform(pattern, -1));
        } else {
            v.vibrate(pattern,-1);
        }
        Log.log("notificationService","notify timer finish");
    }
    private DisplayManager displayManager(){
        return (DisplayManager) getSystemService(Context.DISPLAY_SERVICE);
    }
    public static boolean isScreenOff(DisplayManager dm){
        for (Display display : dm.getDisplays()) {
            if (display.getState() == Display.STATE_ON) {
                Log.log("notificationService","display is on");
                return false;
            }
        }
        Log.log("notificationService","display is off");
        return true;
    }
    private void stop(int startId){
        challengeFinished.set(true);
        stopForeground(true);
        stopSelf(startId);
        Log.log("notificationService","stop service and foreground");
    }
    private void success(){
        Log.log("notificationService","success");
        AttemptRecords.newSuccessRecordNow(getApplicationContext());
    }
    private void noSuccess(){
        Log.log("notificationService","no success");
        AttemptRecords.newNoSuccessRecordNow(getApplicationContext());
    }
    @Override
    public void onDestroy() {
        Log.log("notificationService","service destroyed");
    }
    class NotificationCountdown implements Runnable{
        private String notificationMessage;
        private int notificationId;
        private int startId;
        private boolean isLastOne;
        private NotificationCompat.Builder notificationBuilder;
        public NotificationCountdown(String notificationMessage,int notificationId,int startId,NotificationCompat.Builder notificationBuilder,boolean isLastOne){
            this.notificationMessage=notificationMessage;
            this.notificationId=notificationId;
            this.startId=startId;
            this.notificationBuilder=notificationBuilder;
            this.isLastOne=isLastOne;
        }
        @Override public void run() {
            if(challengeFinished.get()) return;
            updateNotification(notificationBuilder,notificationId,notificationMessage);
            startCheckIfScreenOffAndCheckNotStarted(notificationId,startId);
            if(!isLastOne){
                Log.log("notificationService",notificationMessage);
            }else{
                if(!screenCheckStarted.get()){
                    Log.log("notificationService","stopping");
                    notifyNoSuccess(notificationId);
                    noSuccess();
                    stop(startId);
                }
            }
        }
    }
}