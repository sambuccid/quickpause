package com.sambccd.quickPause;

import android.content.Context;
import android.content.Intent;
import android.hardware.display.DisplayManager;
import android.os.Build;
import com.sambccd.quickPause.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.work.ExistingWorkPolicy;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkInfo;
import androidx.work.WorkManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import com.sambccd.quickPause.attempts.AttemptRecords;
import com.sambccd.quickPause.util.SettingUtils;
import com.sambccd.quickPause.util.TimeScheduleUtil;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

public class SchedulingServiceWorker extends Worker {
    public static final String workerName = "notificationWorkerId";
    public static final String workerRequestTag = "notificationWorker";
    public SchedulingServiceWorker(@NonNull Context context, @NonNull WorkerParameters params) {
        super(context, params);
    }

    @NonNull @Override
    public Result doWork() {
        Log.log("onOff","SchedulingServiceWorker started");

        DisplayManager dm = displayManager();
        if(NotificationService.isScreenOff(dm)){
            challengeAttemptApplied(false,getApplicationContext());
        }else{
            startService(getApplicationContext());
            challengeAttemptApplied(true,getApplicationContext());
        }

        Log.log("onOff","service started");
        return Result.success();
    }

    public static void startService(Context context){
        Intent serviceIntent = new Intent(context, NotificationService.class);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            context.startForegroundService(serviceIntent);
        }else{
            context.startService(serviceIntent);
        }
    }

    private DisplayManager displayManager() {
        return (DisplayManager) getApplicationContext().getSystemService(Context.DISPLAY_SERVICE);
    }

    public static void challengeAttemptApplied(boolean wasStarted, Context context){
        if(wasStarted) {
            registerWorker(context);
        }else{
            postponeWorker(context);
        }
    }
    public static void registerWorker(Context context){//this does a query, it might froze the thread for a bit
        registerWorkerWithPolicy(context,ExistingWorkPolicy.REPLACE);
    }
    public static void registerWorkerIfNotRegistered(Context context){//this does a query, it might froze the thread for a bit
        registerWorkerWithPolicy(context,ExistingWorkPolicy.KEEP);
    }
    public static void registerWorkerWithPolicy(Context context,ExistingWorkPolicy existingPolicy){//this does a query, it might froze the thread for a bit
        registerWorkerParameters(context,null,existingPolicy);
    }
    public static void registerWorkerParameters(Context context,@Nullable Long delayForNextWorker, ExistingWorkPolicy existingPolicy){//this does a query, it might froze the thread for a bit
        int timesAlreadyDone= AttemptRecords.attemptsCountDoneToday(context);
        int timesPerDay = SettingUtils.getTimesPerDay(context);
        Log.log("registerWorker","timesPerDay "+timesPerDay);
        Log.log("registerWorker","timesAlreadyDone "+timesAlreadyDone);
        Log.log("registerWorker","policy  "+existingPolicy);
        if(delayForNextWorker==null){
            delayForNextWorker= TimeScheduleUtil.minutesDelayForNextSchedule(timesPerDay, timesAlreadyDone, context);
        }

        Log.log("registerWorker","minutes for next one "+delayForNextWorker);
        OneTimeWorkRequest notificationWorker = new OneTimeWorkRequest.Builder(SchedulingServiceWorker.class)
                .setInitialDelay(delayForNextWorker, TimeUnit.MINUTES)
                .addTag(workerRequestTag)
                .build();

        WorkManager.getInstance(context).enqueueUniqueWork(workerName, existingPolicy,notificationWorker);

        if(TimeScheduleUtil.isScheduledForTomorrow(delayForNextWorker) && timesPerDay>timesAlreadyDone){
            AttemptRecords.addDailyMissedAttempts(timesPerDay-timesAlreadyDone,context);
        }
    }
    public static void postponeWorker(Context context){//this does a query, it might froze the thread for a bit
        long delayForNextWorker= TimeScheduleUtil.minutesToPostpone;
        Log.log("registerWorker","postpone service");
        registerWorkerParameters(context,delayForNextWorker,ExistingWorkPolicy.APPEND);
    }
    public static void unregisterWorker(Context context){
        WorkManager.getInstance(context).cancelUniqueWork(workerName);
    }
    public static List<WorkInfo> getRegisteredWorkersInfos(Context context){
        List<WorkInfo> infos;
        try {
            infos=WorkManager.getInstance(context).getWorkInfosByTag(workerRequestTag).get();
        } catch (ExecutionException e) {
            throw new RuntimeException(e);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        return infos;
    }
    @Override
    public void onStopped() {
        super.onStopped();
        Log.log("registerWorker","worker stopped");
    }
}
