package com.sambccd.quickPause;

import android.os.Bundle;

import com.sambccd.quickPause.util.SettingUtils;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.navigation.Navigation;

import android.view.View;

import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if(SettingUtils.getActiveStatus(this)==SettingUtils.ActiveStatus.ACTIVE)
            SchedulingServiceWorker.registerWorkerIfNotRegistered(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        /*if (id == R.id.action_points) {
            navigateOnFragment(R.id.open_points_fragment);
            return true;
        }else */if (id == R.id.action_menu_settings) {
            navigateOnFragment(R.id.open_settings_fragment);
            return true;
        }else if(id == R.id.action_guide){
            navigateOnFragment(R.id.open_guide_fragment);
            return true;
        }else if(id == R.id.action_feedback){
            navigateOnFragment(R.id.open_feedback_fragment);
            return true;
        }


        return super.onOptionsItemSelected(item);
    }
    private void navigateOnFragment(int fragmentId){
        View mainView = ((ViewGroup)findViewById(android.R.id.content)).getChildAt(0);
        Navigation.findNavController(this,R.id.nav_host_fragment).navigate(fragmentId);
    }
}