package com.sambccd.quickPause.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.sambccd.quickPause.R;
import com.sambccd.quickPause.attempts.Attempt;
import com.sambccd.quickPause.attempts.AttemptRecords;
import com.sambccd.quickPause.ui.AttemptListAdapter;

import java.util.List;

public class FirstFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_first, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final List<Attempt> attempts = AttemptRecords.getAllAttempts(getActivity().getApplicationContext());
        final RecyclerView listView=loadAttemptList(view, attempts);

        view.findViewById(R.id.button_first).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(FirstFragment.this)
                        .navigate(R.id.action_FirstFragment_to_SecondFragment);
            }
        });
        view.findViewById(R.id.deleteAllAttemptsBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new Thread(new DeleteAllRecordsRunnable(getActivity().getApplicationContext())).start();
                int size=attempts.size();
                attempts.clear();
                listView.getAdapter().notifyItemRangeRemoved(0,size);
            }
        });
    }

    private RecyclerView loadAttemptList(View mainView, List<Attempt> attempts) {
        RecyclerView listView = mainView.findViewById(R.id.attemptsList);
        initListView(listView);
        listView.setAdapter(new AttemptListAdapter(attempts));
        return listView;
    }

    private void initListView(RecyclerView listView){
        listView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        listView.setLayoutManager(layoutManager);
    }
    static class DeleteAllRecordsRunnable implements Runnable {
        private final Context context;
        public DeleteAllRecordsRunnable(Context context){
            this.context=context;
        }
        @Override
        public void run() {
            AttemptRecords.deleteAll(context);
        }
    }
}