package com.sambccd.quickPause.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.sambccd.quickPause.R;
import com.sambccd.quickPause.SchedulingServiceWorker;
import com.sambccd.quickPause.util.SettingUtils;

public class MainFragment extends Fragment {
    public MainFragment() {

    }
    public static MainFragment newInstance() {
        MainFragment fragment = new MainFragment();
        return fragment;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_main, container, false);
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setStatusComponents(view, SettingUtils.getActiveStatus(getActivity().getApplicationContext()));
        setButtonsClick(view);
    }
    private void setStatusComponents(View mainView, SettingUtils.ActiveStatus status){
        TextView statusTxt = mainView.findViewById(R.id.statusTxt);
        TextView switchStatusBtn = mainView.findViewById(R.id.switchStatusBtn);
        FrameLayout statusLyt = mainView.findViewById(R.id.statusLayout);
        if(status==SettingUtils.ActiveStatus.ACTIVE){
            statusTxt.setText(getString(R.string.statusTxtEnabled));
            statusLyt.setBackgroundColor(ContextCompat.getColor(getContext(),R.color.colorStatusEnabled));
            switchStatusBtn.setText(getString(R.string.statusBtnWhileEnabled));
        }else{
            statusTxt.setText(getString(R.string.statusTxtDisabled));
            statusLyt.setBackgroundColor(ContextCompat.getColor(getContext(),R.color.colorStatusDisabled));
            switchStatusBtn.setText(getString(R.string.statusBtnWhileDisabled));
        }
    }
    private void setButtonsClick(final View mainView){
        mainView.findViewById(R.id.switchStatusBtn).setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View view) {
                SettingUtils.ActiveStatus newStatus = switchActiveStatus();
                setStatusComponents(mainView,newStatus);
            }
            private SettingUtils.ActiveStatus switchActiveStatus() {
                SettingUtils.ActiveStatus newStatus = SettingUtils.switchActiveStatus(getActivity().getApplicationContext());
                if(newStatus==SettingUtils.ActiveStatus.ACTIVE){
                    SchedulingServiceWorker.registerWorker(getActivity().getApplicationContext());
                }else{
                    SchedulingServiceWorker.unregisterWorker(getActivity().getApplicationContext());
                }
                return newStatus;
            }
        });
        mainView.findViewById(R.id.settingsBtn).setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View view) {
                Navigation.findNavController(getActivity(),R.id.nav_host_fragment).navigate(R.id.open_settings_fragment);
            }
        });
        //commented and not deleted because are they are used during development and test
        /*mainView.findViewById(R.id.otherBtn).setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View view) {
                Navigation.findNavController(getActivity(),R.id.nav_host_fragment).navigate(R.id.action_mainFragment_to_FirstFragment);
            }
        });
        mainView.findViewById(R.id.testButton).setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View view) {
                RunOnceAWhile.startService(getActivity().getApplicationContext());
            }
        });*/
    }
}