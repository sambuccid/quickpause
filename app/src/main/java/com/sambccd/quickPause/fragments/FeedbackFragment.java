package com.sambccd.quickPause.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sambccd.quickPause.R;

public class FeedbackFragment extends Fragment {
    public FeedbackFragment() {}
    public static FeedbackFragment newInstance() {
        return new FeedbackFragment();
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_feedback, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        view.findViewById(R.id.feedbackBtn).setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View view) {
                openEmailSend();
            }
        });
    }
    private void openEmailSend(){
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto","sambccd@gmail.com", null));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Application feedback");
        startActivity(Intent.createChooser(emailIntent, "Send feedback"));
    }
}