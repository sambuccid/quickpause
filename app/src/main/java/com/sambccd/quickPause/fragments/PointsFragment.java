package com.sambccd.quickPause.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sambccd.quickPause.R;
import com.sambccd.quickPause.attempts.Attempt;
import com.sambccd.quickPause.attempts.AttemptRecords;

import java.util.List;

public class PointsFragment extends Fragment {
    public static PointsFragment newInstance() {
        PointsFragment fragment = new PointsFragment();
        return fragment;
    }
    public PointsFragment() {}
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_points, container, false);
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final List<Attempt> attempts = AttemptRecords.getAllAttempts(getActivity().getApplicationContext());
        final long numMissedAttempts=AttemptRecords.countMissedAttempts(getActivity().getApplicationContext());
        long points=calculatePoints(attempts,numMissedAttempts);
        TextView pointsTxt=view.findViewById(R.id.pointsTxt);
        pointsTxt.setText(String.valueOf(points));
    }
    public static long calculatePoints(List<Attempt> attempts,long missedAttemptsNum){
        long points=0;
        for(Attempt attempt:attempts){
            long pointsForSuccess = ((attempt.seconds/5)-1)*5;
            if(attempt.success)
                points+=pointsForSuccess;
            else
                points-=   pointsForSuccess - ((attempt.seconds/5)-1);
        }
        points+=missedAttemptsNum*4;
        return Math.max(points,0);
    }
}