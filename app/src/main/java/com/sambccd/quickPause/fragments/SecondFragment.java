package com.sambccd.quickPause.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.work.WorkInfo;

import com.sambccd.quickPause.R;
import com.sambccd.quickPause.SchedulingServiceWorker;
import com.sambccd.quickPause.ui.WorkersListAdapter;

import java.util.List;

public class SecondFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_second, container, false);
    }
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        List<WorkInfo> infos= SchedulingServiceWorker.getRegisteredWorkersInfos(getContext());
        final RecyclerView listView=loadStatesList(view, infos);
        view.findViewById(R.id.button_second).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(SecondFragment.this)
                        .navigate(R.id.action_SecondFragment_to_FirstFragment);
            }
        });
    }
    private RecyclerView loadStatesList(View mainView, List<WorkInfo> infos) {
        RecyclerView listView = mainView.findViewById(R.id.plannedWorkersList);
        initListView(listView);
        listView.setAdapter(new WorkersListAdapter(infos));
        return listView;
    }
    private void initListView(RecyclerView listView){
        listView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        listView.setLayoutManager(layoutManager);
    }
}
