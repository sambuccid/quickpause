package com.sambccd.quickPause.fragments;

import android.os.Bundle;

import androidx.preference.PreferenceFragmentCompat;

import com.sambccd.quickPause.R;
import com.sambccd.quickPause.SchedulingServiceWorker;
import com.sambccd.quickPause.util.SettingUtils;

import java.util.Calendar;

public class SettingFragment extends PreferenceFragmentCompat {
    private Integer initialTimesPerDay=null;
    private Calendar initialStartUseDayTime=null;
    private Calendar initialEndUseDayTime=null;
    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        initialTimesPerDay= SettingUtils.getTimesPerDay(getContext());
        initialStartUseDayTime= SettingUtils.startUseDayTime(getContext());
        initialEndUseDayTime= SettingUtils.endUseDayTime(getContext());
        setPreferencesFromResource(R.xml.preferences,rootKey);
    }
    @Override
    public void onPause(){
        super.onPause();
        int currentTimePerDay=SettingUtils.getTimesPerDay(getContext());
        Calendar currentStartUseDayTime= SettingUtils.startUseDayTime(getContext());
        Calendar currentEndUseDayTime= SettingUtils.endUseDayTime(getContext());
        if(initialTimesPerDay==null || !initialTimesPerDay.equals(currentTimePerDay)
                    || initialStartUseDayTime==null || initialStartUseDayTime.getTimeInMillis()!=currentStartUseDayTime.getTimeInMillis()
                    || initialEndUseDayTime==null || initialEndUseDayTime.getTimeInMillis()!=currentEndUseDayTime.getTimeInMillis()){
            SchedulingServiceWorker.registerWorker(getContext());
            initialTimesPerDay=currentTimePerDay;
        }
    }
}
