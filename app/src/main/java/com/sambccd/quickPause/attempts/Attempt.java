package com.sambccd.quickPause.attempts;

import java.util.Calendar;

public class Attempt {
    public Attempt(Calendar date,int seconds,boolean success){
        this.date=date;
        this.seconds=seconds;
        this.success=success;
    }
    public Calendar date;
    public int seconds;
    public boolean success;
}
