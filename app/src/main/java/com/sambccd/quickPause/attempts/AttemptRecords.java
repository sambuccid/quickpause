package com.sambccd.quickPause.attempts;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.sambccd.quickPause.db.DbHelper;
import com.sambccd.quickPause.util.SettingUtils;

import java.util.Calendar;
import java.util.List;

public class AttemptRecords {
    public static void newSuccessRecordNow(Context context) {
        DbHelper dbH = new DbHelper(context);
        SQLiteDatabase db = dbH.getWritableDatabase();
        int seconds=SettingUtils.getSecondsScreenOff(context);
        AttemptsRecordsDAO.addNewRecord(Calendar.getInstance(),seconds,true, db);
        db.close();
    }
    public static void newNoSuccessRecordNow(Context context) {
        DbHelper dbH = new DbHelper(context);
        SQLiteDatabase db = dbH.getWritableDatabase();
        int seconds=SettingUtils.getSecondsScreenOff(context);
        AttemptsRecordsDAO.addNewRecord(Calendar.getInstance(),seconds,false, db);
        db.close();
    }
    public static long newRecordNow(Context context){
        DbHelper dbH = new DbHelper(context);
        SQLiteDatabase db = dbH.getWritableDatabase();
        int seconds=SettingUtils.getSecondsScreenOff(context);
        long newId = AttemptsRecordsDAO.addNewRecord(Calendar.getInstance(),seconds,false, db);
        db.close();
        return newId;
    }
    public static List<Calendar> getAllAttemptsDates(Context context) {
        DbHelper dbH = new DbHelper(context);
        SQLiteDatabase db = dbH.getReadableDatabase();
        List<Calendar> results = AttemptsRecordsDAO.getAllDates(db);
        db.close();
        return results;
    }public static List<Attempt> getAllAttempts(Context context) {
        DbHelper dbH = new DbHelper(context);
        SQLiteDatabase db = dbH.getReadableDatabase();
        List<Attempt> results=AttemptsRecordsDAO.getAllAttempts(db);
        db.close();
        return results;
    }
    public static int attemptsCountDoneToday(Context context){
        DbHelper dbH = new DbHelper(context);
        SQLiteDatabase db = dbH.getReadableDatabase();
        int counts=AttemptsRecordsDAO.attemptsCountDoneToday(db);
        db.close();
        return counts;
    }
    public static void setAttemptSuccess(long attemptIdToUpdate, Context context) {
        DbHelper dbH = new DbHelper(context);
        SQLiteDatabase db = dbH.getWritableDatabase();
        AttemptsRecordsDAO.updateRecordSuccess(attemptIdToUpdate,true, db);
        db.close();
    }
    public static void setAttemptNoSuccess(long attemptIdToUpdate, Context context) {
        DbHelper dbH = new DbHelper(context);
        SQLiteDatabase db = dbH.getWritableDatabase();
        AttemptsRecordsDAO.updateRecordSuccess(attemptIdToUpdate,false, db);
        db.close();
    }
    public static void deleteAttempt(long attemptIdToUpdate, Context context){
        DbHelper dbH = new DbHelper(context);
        SQLiteDatabase db = dbH.getWritableDatabase();
        AttemptsRecordsDAO.deleteAttempt(attemptIdToUpdate,db);
        db.close();
    }
    public static void deleteAll(Context context) {
        DbHelper dbH = new DbHelper(context);
        SQLiteDatabase db = dbH.getWritableDatabase();
        AttemptsRecordsDAO.deleteAll(db);
        db.close();
    }
    public static void addDailyMissedAttempts(int missedAttempts,Context context) {
        DbHelper dbH = new DbHelper(context);
        SQLiteDatabase db = dbH.getWritableDatabase();
        int seconds=SettingUtils.getSecondsScreenOff(context);
        int supposedAttempts=SettingUtils.getTimesPerDay(context);
        MissedAttemptsDAO.addMissedAttempts(missedAttempts,Calendar.getInstance(),seconds,supposedAttempts,db);
        db.close();
    }
    public static long countMissedAttempts(Context context) {
        DbHelper dbH = new DbHelper(context);
        SQLiteDatabase db = dbH.getReadableDatabase();
        long result=MissedAttemptsDAO.countMissedNum(db);
        db.close();
        return result;
    }
}
