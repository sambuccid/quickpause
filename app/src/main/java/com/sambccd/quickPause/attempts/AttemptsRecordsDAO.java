package com.sambccd.quickPause.attempts;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;

import com.sambccd.quickPause.util.TimeUtil;
import com.sambccd.quickPause.db.DbHelper;
import com.sambccd.quickPause.db.TableNames;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class AttemptsRecordsDAO{
    public static long addNewRecord(Calendar date, int seconds,boolean success, SQLiteDatabase db){
        ContentValues row = new ContentValues();
        row.put(TableNames.Attempt.Fields.date, DbHelper.dateVal(date));
        row.put(TableNames.Attempt.Fields.seconds, seconds);
        row.put(TableNames.Attempt.Fields.success, DbHelper.boolVal(success));
        long id = db.insert(TableNames.Attempt.name, null, row);
        if(id==-1)
            throw new RuntimeException("There was an error inserting the values for attempts");
        return id;
    }
    public static List<Calendar> getAllDates(SQLiteDatabase db) {
        String[] columns={TableNames.Attempt.Fields.date};
        Cursor cRes=db.query(
                TableNames.Attempt.name,
                columns,
                null,
                null,
                null,
                null,
                TableNames.Attempt.Fields.date
        );
        List<Calendar> results = new ArrayList<>();
        while (cRes.moveToNext()){
            long time=cRes.getLong(0);
            results.add(DbHelper.dateVal(time));
        }
        return results;
    }
    public static List<Attempt> getAllAttempts(SQLiteDatabase db) {
        String[] columns={
                TableNames.Attempt.Fields.date,
                TableNames.Attempt.Fields.seconds,
                TableNames.Attempt.Fields.success};
        Cursor cRes=db.query(
                TableNames.Attempt.name,
                columns,
                null,
                null,
                null,
                null,
                TableNames.Attempt.Fields.date
        );
        List<Attempt> results = new ArrayList<>();
        while (cRes.moveToNext()){
            long timemills=cRes.getLong(0);
            Calendar time = DbHelper.dateVal(timemills);
            int seconds=cRes.getInt(1);
            int successInt=cRes.getInt(2);
            boolean success=DbHelper.boolVal(successInt);
            results.add(new Attempt(time,seconds,success));
        }
        return results;
    }
    public static int attemptsCountDoneToday(SQLiteDatabase db){
        String filter= TableNames.Attempt.Fields.date+" BETWEEN "+ TimeUtil.todayStartInMills()+" AND "+TimeUtil.todayEndInMills();
        long count=DatabaseUtils.queryNumEntries(db,TableNames.Attempt.name,filter);
        return (int)count;
    }
    public static void updateRecordSuccess(long attemptIdToUpdate, boolean success, SQLiteDatabase db) {
        ContentValues newVal = new ContentValues();
        newVal.put(TableNames.Attempt.Fields.success, DbHelper.boolVal(success));
        String where = TableNames.Attempt.Fields.id+"="+attemptIdToUpdate;
        int rowsUpdated=db.update(TableNames.Attempt.name,newVal,where,null);
        if(rowsUpdated<1) throw new RuntimeException("The passed id was not found in the db");
        if(rowsUpdated>1) throw new RuntimeException("The update function has found multiple rows with the where clause, this is not expected");
    }
    public static void deleteAttempt(long attemptIdToUpdate, SQLiteDatabase db) {
        String where = TableNames.Attempt.Fields.id+"="+attemptIdToUpdate;
        int rowsDeleted=db.delete(TableNames.Attempt.name,where,null);
        if(rowsDeleted<1) throw new RuntimeException("The passed id was not found in the db");
        if(rowsDeleted>1) throw new RuntimeException("The delete function has found multiple rows with the where clause, this is not expected");
    }
    public static void deleteAll(SQLiteDatabase db) {
        db.delete(TableNames.Attempt.name,null,null);
    }
}
