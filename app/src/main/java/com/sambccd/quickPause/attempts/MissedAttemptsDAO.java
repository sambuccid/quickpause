package com.sambccd.quickPause.attempts;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.sambccd.quickPause.db.DbHelper;
import com.sambccd.quickPause.db.TableNames;

import java.util.Calendar;

public class MissedAttemptsDAO {
    public static long addMissedAttempts(int missedAttempts, Calendar date, int seconds, int supposedAttempts, SQLiteDatabase db) {
        ContentValues row = new ContentValues();
        row.put(TableNames.MissedAttempts.Fields.missingAttempts, DbHelper.boolVal(missedAttempts));
        row.put(TableNames.MissedAttempts.Fields.date, DbHelper.dateVal(date));
        row.put(TableNames.MissedAttempts.Fields.seconds, seconds);
        row.put(TableNames.MissedAttempts.Fields.supposedAttempts, DbHelper.boolVal(supposedAttempts));
        long id = db.insert(TableNames.MissedAttempts.name, null, row);
        if(id==-1)
            throw new RuntimeException("There was an error inserting the values for missed attempts");
        return id;
    }
    public static long countMissedNum(SQLiteDatabase db) {
        String query = "SELECT sum("+TableNames.MissedAttempts.Fields.missingAttempts+") " +
                        "FROM "+TableNames.MissedAttempts.name+" ";
        Cursor cRes=db.rawQuery(query,null);
        if(cRes.getCount()!=1) throw new RuntimeException("Something gone wrong result number not expected");
        if(cRes.moveToFirst())
            return cRes.getLong(0);
        return 0;
    }
}
