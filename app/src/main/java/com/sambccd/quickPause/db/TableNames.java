package com.sambccd.quickPause.db;

public class TableNames {
    public static class Attempt{
        public static final String name="attempt";
        public static final String createString="CREATE TABLE attempt (" +
                                                                "_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
                                                                "date int NOT NULL, " +
                                                                "seconds int NOT NULL, " +
                                                                "success int NOT NULL)";
        public static class Fields{
            public static final String id="_id";
            public static final String date ="date";
            public static final String seconds ="seconds";
            public static final String success ="success";
        }
    }
    public static class MissedAttempts {
        public static final String name="missedAttempts";
        public static final String createString="CREATE TABLE missedAttempts (" +
                "_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
                "missingAttempts int NOT NULL, " +
                "date int NOT NULL, " +
                "seconds int NOT NULL, " +
                "supposedAttempts int NOT NULL)";
        public static class Fields{
            public static final String id="_id";
            public static final String missingAttempts ="missingAttempts";
            public static final String date ="date";
            public static final String seconds ="seconds";
            public static final String supposedAttempts ="supposedAttempts";
        }
    }
}
