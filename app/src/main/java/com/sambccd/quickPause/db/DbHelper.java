package com.sambccd.quickPause.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.Calendar;

public class DbHelper extends SQLiteOpenHelper {
    public static final int dbVersion=1;
    public static final String dbName="Database";
    public DbHelper(Context context) {
        super(context, dbName, null, dbVersion);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(TableNames.Attempt.createString);
        db.execSQL(TableNames.MissedAttempts.createString);
    }
    @Override
    @Deprecated
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        throw new UnsupportedOperationException("the db never thought to be updated, please implement this");
    }

    public static int boolVal(boolean bool){
        return bool? 1 : 0;
    }
    public static boolean boolVal(int bool){
        if(bool!=1 && bool!=0) throw new RuntimeException("Value not supported as boolean");
        return bool==1;
    }
    public static long dateVal(Calendar date){
        return date.getTimeInMillis();
    }
    public static Calendar dateVal(long date){
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(date);
        return c;
    }
}
