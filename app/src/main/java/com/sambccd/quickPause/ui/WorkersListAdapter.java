package com.sambccd.quickPause.ui;

import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.work.WorkInfo;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

public class WorkersListAdapter extends RecyclerView.Adapter<WorkersListAdapter.ItemHolder>{
    public static class ItemHolder extends RecyclerView.ViewHolder {
        public TextView textView;
        public ItemHolder(TextView v) {
            super(v);
            textView = v;
        }
    }
    private List<WorkInfo> infos=null;
    public WorkersListAdapter(List<WorkInfo> infos){
        this.infos=infos;
    }
    @NonNull
    @Override public ItemHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        TextView item = new TextView(parent.getContext());
        ItemHolder itemHolder = new ItemHolder(item);
        return itemHolder;
    }
    @Override public void onBindViewHolder(@NonNull ItemHolder itemHolder, int position) {
        String text = infos.get(position).getState().name();
        itemHolder.textView.setText(text);
    }
    @Override public int getItemCount() {
        return infos.size();
    }
    private String formatDate(Calendar date){
        return dateFormatter.format(date.getTime());
    }
    private SimpleDateFormat dateFormatter=new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
}
