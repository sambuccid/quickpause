package com.sambccd.quickPause.ui;

import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sambccd.quickPause.attempts.Attempt;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

public class AttemptListAdapter extends RecyclerView.Adapter<AttemptListAdapter.ItemHolder>{
    public static class ItemHolder extends RecyclerView.ViewHolder {
        public TextView textView;
        public ItemHolder(TextView v) {
            super(v);
            textView = v;
        }
    }
    private List<Attempt> attempts=null;
    public AttemptListAdapter(List<Attempt> attempts){
        this.attempts=attempts;
    }
    @NonNull
    @Override public ItemHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        TextView item = new TextView(parent.getContext());
        ItemHolder itemHolder = new ItemHolder(item);
        return itemHolder;
    }
    @Override public void onBindViewHolder(@NonNull ItemHolder itemHolder, int position) {
        String text = formatDate(attempts.get(position).date)+" - "+attempts.get(position).success;
        itemHolder.textView.setText(text);
    }
    @Override public int getItemCount() {
        return attempts.size();
    }
    private String formatDate(Calendar date){
        return dateFormatter.format(date.getTime());
    }
    private SimpleDateFormat dateFormatter=new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
}
