package com.sambccd.quickPause.util;

import java.util.Calendar;

public class TimeUtil {
    public static long todayStartInMills(){
        return timeAtTodayHour(0).getTimeInMillis();
    }
    public static long todayEndInMills(){
        return timeAtTodayHour(24).getTimeInMillis();
    }
    public static Calendar timeAtTodayHour(int hour){
        Calendar c= Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY,hour);
        c.set(Calendar.MINUTE,0);
        c.set(Calendar.SECOND,0);
        c.set(Calendar.MILLISECOND,0);
        return c;
    }
    public static boolean momentPassed(long millsMoment){
        return Calendar.getInstance().getTimeInMillis()>millsMoment;
    }
}
