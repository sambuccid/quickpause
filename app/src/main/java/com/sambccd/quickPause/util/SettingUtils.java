package com.sambccd.quickPause.util;

import android.content.Context;
import android.content.SharedPreferences;
import androidx.preference.PreferenceManager;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import static com.sambccd.quickPause.util.SettingUtils.ActiveStatus.*;

public class SettingUtils {
    public static int getTimesPerDay(Context context){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getInt("difficulty",1) * 2;
    }
    public static int getSecondsScreenOff(Context context){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getInt("SecondsScreenOff",10);
    }
    public static WayNotifyEndChallenge getWayNotifyEndChallenge(Context context){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        String keyPreference=prefs.getString("vibrationSoundNotifyEndChallenge","");
        return WayNotifyEndChallenge.getFromKeyProperty(keyPreference);
    }
    public static ActiveStatus getActiveStatus(Context context){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        String keyPreference=prefs.getString("activeStatus","active");
        return ActiveStatus.getFromKeyProperty(keyPreference);
    }
    public static ActiveStatus switchActiveStatus(Context context){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        String keyPreference=prefs.getString("activeStatus",ActiveStatus.defaultValue);
        ActiveStatus currentStatus = ActiveStatus.getFromKeyProperty(keyPreference);
        ActiveStatus newStatus = currentStatus==ACTIVE? INACTIVE : ACTIVE;
        prefs.edit().putString("activeStatus",ActiveStatus.getKeyProperty(newStatus)).apply();
        return newStatus;
    }
    public static boolean vibrationAtStart(Context context){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getBoolean("vibrationAtStartChallenge",true);
    }
    public static Calendar startUseDayTime(Context context){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        String hourStr = prefs.getString("hourStartUseDay","4");
        int hour = Integer.parseInt(hourStr);
        return TimeUtil.timeAtTodayHour(hour);
    }
    public static Calendar endUseDayTime(Context context){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        String hourStr = prefs.getString("hourEndUseDay","23");
        int hour = Integer.parseInt(hourStr);
        return TimeUtil.timeAtTodayHour(hour);
    }
    public enum WayNotifyEndChallenge{//values from arrays.xml, keep in sync
        SOUND(true,false),
        VIBRATION(false,true),
        SOUNDANDVIBRATION(true,true);
        public final boolean soundEnable;
        public final boolean vibrationEnable;
        WayNotifyEndChallenge(boolean soundEnable, boolean vibrationEnable){
            this.soundEnable=soundEnable;
            this.vibrationEnable=vibrationEnable;
        }
        public static WayNotifyEndChallenge getFromKeyProperty(String keyProp){
            if(keyProp.equals("sound")) {
                return WayNotifyEndChallenge.SOUND;
            }else if(keyProp.equals("vibration")) {
                return WayNotifyEndChallenge.VIBRATION;
            }else if(keyProp.equals("soundVibration")) {
                return WayNotifyEndChallenge.SOUNDANDVIBRATION;
            }else
                throw new RuntimeException("Passed key not found");
        }
    }
    public enum ActiveStatus{
        ACTIVE,
        INACTIVE;
        private static final Map<String,ActiveStatus> keyPropEnumMap=new HashMap<>();
        public static final String defaultValue = "active";
        static{
            keyPropEnumMap.put("active",ACTIVE);
            keyPropEnumMap.put("inactive",INACTIVE);
        }
        public static ActiveStatus getFromKeyProperty(String keyProp){
            return keyPropEnumMap.get(keyProp);
        }
        public static String getKeyProperty(ActiveStatus status){
            for(Map.Entry<String,ActiveStatus> entry : keyPropEnumMap.entrySet()){
                if(entry.getValue()==status)
                    return entry.getKey();
            }
            throw new RuntimeException("Value not found in the map, so the map is probably not complete");
        }
    }
}
