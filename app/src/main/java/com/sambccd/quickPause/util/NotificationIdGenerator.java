package com.sambccd.quickPause.util;

public class NotificationIdGenerator {
    private static int lastId=1;
    public static int generateId(){
        lastId++;
        return lastId;
    }
}
