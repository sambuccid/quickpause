package com.sambccd.quickPause.util;

import android.content.Context;

import java.util.Calendar;
import java.util.concurrent.TimeUnit;

public class TimeScheduleUtil {
    public static final int minimMinutes = 2;
    public static final int minutesToPostpone = 5;
    public static final int edgeMinutesForUnexpected = 20;
    public static final boolean testing=false;
    public static final int testingMin=1;
    public static long minutesDelayForNextSchedule(int timesPerDay, int timesAlreadyDone, Context context){
        if(testing)
            return testingMin;
        if(timesAlreadyDone>=timesPerDay)
            return scheduleForTomorrow(context);
        //take now time(in local time)
        Calendar now = Calendar.getInstance();
        //take range for what would be initial and end of the day when the phone is used
        Calendar endOfPhoneUsedDay = getEndUsageDay(context);
        //calculate the remaining time
        long remainingMillsToEndUseDay = endOfPhoneUsedDay.getTimeInMillis() - now.getTimeInMillis();
        //check if the schedule should be placed for the next day(it's too late or all the times are already done or things like that)
        if(isTooLate(remainingMillsToEndUseDay))
            return scheduleForTomorrow(context);
        //remove a portion of the remaining time for when the job should be delayed(the phone is off)
        remainingMillsToEndUseDay -= minutesToMills(edgeMinutesForUnexpected);
        //divide the time by the times left to be done
        int remainingTimes=timesPerDay-timesAlreadyDone;
        long delayInMills = remainingMillsToEndUseDay/(remainingTimes+1);
        //convert the result in minutes
        //return them
        return millsToMinutestWithMinimum(delayInMills);
    }
    public static long minutesToMills(int minutes){
        return TimeUnit.MINUTES.toMillis(minutes);
    }
    public static int millsToMinutes(long mills){
        return (int) TimeUnit.MILLISECONDS.toMinutes(mills);
    }
    public static long secondsToMills(long secs){
        return (int) TimeUnit.SECONDS.toMillis(secs);
    }
    public static Calendar getEndUsageDay(Context context){
        Calendar endUsageDay = SettingUtils.endUseDayTime(context);
        return endUsageDay;
    }
    private static int scheduleForTomorrow(Context context){
        Calendar tomorrowTime = SettingUtils.startUseDayTime(context);
        tomorrowTime.add(Calendar.DAY_OF_MONTH,1);
        Calendar now = Calendar.getInstance();
        long millisToTomorrowMorning = tomorrowTime.getTimeInMillis() - now.getTimeInMillis();
        return millsToMinutestWithMinimum(millisToTomorrowMorning);
    }
    private static boolean isTooLate(long timeWeHave){
        return timeWeHave <= minutesToMills(minimMinutes);
    }
    private static int millsToMinutestWithMinimum(long mills){
        int minutes = millsToMinutes(mills);
        if(minutes<minimMinutes)
            return minimMinutes;
        else
            return minutes;
    }
    public static boolean isScheduledForTomorrow(long mills){
        Calendar a=Calendar.getInstance();
        a.add(Calendar.MILLISECOND,(int)mills);
        return a.after(TimeUtil.todayEndInMills());
    }
}
